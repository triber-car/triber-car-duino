#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define USE_OBD 1
#define USE_MEMS 1
#define ENABLE_ORIENTATION 1
#define USE_GPS 1
#define USE_WIFI 1
#define USE_SD 1

#define MEMS_ACC 1

#define GPS_SERIAL_BAUDRATE 115200L
#define OBD_ATTEMPT_TIME 180000
#define SD_CS_PIN 5

#if USE_WIFI
#include "wifiConfig.h"
// this file should contain
// #define WIFI_SSID 
// #define WIFI_PASSWORD 
// #define API_KEY 
// #define SERVER_URL 
// #define SERVER_PORT 
#endif

#endif