#include <Arduino.h>
#include <FreematicsONE.h>
#include "config.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#define PIN_LED 4

typedef struct {
    bool empty;
    uint32_t date;
    uint32_t time;
    int32_t lat;
    int32_t lng;
    int16_t alt;
    uint8_t speed;
    uint8_t sat;
    int16_t heading;
} TRIBER_GPS_DATA;

typedef struct {
    bool empty;
    int16_t load;
    int16_t coolant;
    int16_t rpm;
    int16_t speed;
    int16_t intakeTemp;
    int16_t throttle;
    int16_t aux;
    int16_t runtime;
    int16_t fuel;
    int16_t baro;
    int16_t temp;
    int16_t pedal;
    int16_t bat;
} TRIBER_OBD_DATA;

typedef struct {
    bool empty;
    int16_t x;
    int16_t y;
    int16_t z;
} TRIBER_MEMS_DATA;

class CarDuino : public COBDSPI{
public:
    bool obdReady = false;
    bool wifiReady = false;
    bool gpsReady = false;
    bool memsReady = false;
    bool sdReady = false;
    char vin[128];
    SDClass SD;
    WiFiClient client;
    char path[25];
    void setup() {
        Serial.println("Setting up");
        setupOBD();
        setupGPS();
        setupMEMS();
        setupWIFI();
        setupSD();
    }

    TRIBER_MEMS_DATA readMEMS() {
        TRIBER_MEMS_DATA result = {0};
        result.empty = true;
        if(!memsReady) {
            setupMEMS();
        }
        if(memsReady) {
            Serial.println("Getting MEMS data");
            ORIENTATION ori;
            float acc[3];
            mems.memsRead(acc, 0, 0, 0, &ori);
            result.x = (int16_t)(acc[0] * 100);
            result.y = (int16_t)(acc[1] * 100);
            result.z = (int16_t)(acc[2] * 100);
            result.empty = false;
        }
        return result;
    }

    TRIBER_GPS_DATA readGPS() {
        TRIBER_GPS_DATA result = {0};
        result.empty = true;
        if(!gpsReady) {
            setupGPS();
        }
        if(gpsReady) {
            Serial.println("Getting GPS data");
            GPS_DATA gd = {0};
            if(gpsGetData(&gd)) {
                if (gd.time) {
                    result.date = gd.date;
                    result.time = gd.time;
                    result.empty = false;
                    result.lat = gd.lat;
                    result.lng = gd.lng;
                    result.alt = gd.alt;
                    result.speed = gd.speed;
                    result.sat = gd.sat;
                    result.heading = gd.heading;
                    previousGPS = gd.time;
                }
            }
        }
        return result;
    }

    TRIBER_OBD_DATA readOBD() {
        TRIBER_OBD_DATA result = {0};
        result.empty = true;
        if(!obdReady) {
            setupOBD();
        }
        if(obdReady) {
            Serial.println("Getting OBD data");
            int value;
            if(readPID(PID_ENGINE_LOAD, value)) {
                result.load = value;
            }
            if(readPID(PID_COOLANT_TEMP, value)) {
                result.coolant = value;
            }
            if(readPID(PID_RPM, value)) {
                result.rpm = value;
            }
            if(readPID(PID_SPEED, value)) {
                result.speed = value;
            } 
            if(readPID(PID_INTAKE_TEMP, value)) {
                result.intakeTemp = value;
            }
            if(readPID(PID_THROTTLE, value)) {
                result.throttle = value;
            }
            if(readPID(PID_AUX_INPUT, value)) {
                result.aux = value;
            }
            if(readPID(PID_RUNTIME, value)) {
                result.runtime = value;
            }
            if(readPID(PID_FUEL_LEVEL, value)) {
                result.runtime = value;
            }
            if(readPID(PID_BAROMETRIC, value)) {
                result.baro = value;
            }
            if(readPID(PID_AMBIENT_TEMP, value)) {
                result.temp = value;
            }
            if(readPID(PID_ACC_PEDAL_POS_D, value)) {
                result.pedal = value;
            }
            result.bat = getVoltage() * 100;
            result.empty = false;
        }
        return result;
    }

    void writeToSD(String data) {
        if(!sdReady) {
            setupSD();
        }
        if(!sdReady) return;
        Serial.print("Writing to SD file ");
        File file = SD.open(path, FILE_WRITE );
        if(!file) {
            Serial.println("NOK");
            return;
        }
        file.print(data);
        file.println(";");
        file.close();
        Serial.println("OK");
    }

    void sendWifi(String data) {
        #if USE_WIFI
        if(!wifiReady) {
            setupWIFI();
        }
        if(!wifiReady) return;
        Serial.print("Sending to server ");
        if(client.connect(SERVER_URL, SERVER_PORT)) {
            client.println("POST / HTTP/1.1");
            client.println("Host: carduino");
            client.println("User-Agent: Carduino/1.0");
            client.print("Api-Key: ");
            client.println(API_KEY);
            client.println("Connection: close");
            client.println("Content-Type: application/json");
            client.print("Content-Length: ");
            client.println(data.length());
            client.println();
            client.println(data);
            client.stop();
            Serial.println("OK");
        } else {
            Serial.println("NOK");
        }
        #endif
    }
private:
    MPU9250_9DOF mems;
    uint32_t previousGPS = 0;
    void setupOBD() {
        #if USE_OBD
        Serial.print("Setting up OBD ");
        if(init()) {
            Serial.println("OK");
            obdReady = true;
            getVIN(vin, sizeof(vin));
        } else {
            Serial.println("NOK");
            obdReady = false;
        }
        #else
            Serial.println("OBD disabled");
        #endif
    }

    void setupGPS() {
        #if USE_GPS
        Serial.print("Setting up GPS ");
        if(gpsInit(GPS_SERIAL_BAUDRATE)) {
            gpsReady = true;
            Serial.println("OK");
            fixGPS();
        } else {
            Serial.println("NOK");
        }
        #else
        Serial.println("GPS disabled");
        #endif
    }

    void fixGPS() {
        Serial.print("Getting a gps fix");
        GPS_DATA gd = {0};
        for(int attempt = 0; attempt < 5; attempt++) {
            Serial.print(".");
            if(gpsGetData(&gd) && gd.sat != 0 && gd.sat != 255) {
                Serial.println("SATS: ");
                Serial.println(gd.sat);
                break;
            }
            delay(1000);
        }
        Serial.println();
    }

    void setupWIFI() {
        #if USE_WIFI
        WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
        Serial.print("Connecting to Wifi");
        for(int attempt = 0; attempt < 5 && WiFi.status() != WL_CONNECTED; attempt ++) {
            delay(1000);
            Serial.print(".");
        }
        if(WiFi.status() == WL_CONNECTED) {
            Serial.println("OK");
            Serial.print("Ip: ");
            Serial.println(WiFi.localIP());
            wifiReady = true;
        } else {
            Serial.println("NOK");
            wifiReady = false;
        }
        #else
        Serial.println("WIFI disabled");
        #endif
    }

    void setupMEMS() {
        #if USE_MEMS
        Serial.print("Setting up MEMS ");
        if(mems.memsInit(ENABLE_ORIENTATION)) {
            memsReady = true;
            Serial.println("OK");
        } else {
            memsReady = false;
            Serial.println("NOK");
        }
        #else
        Serial.println("MEMS disabled");
        #endif
    }

    void setupSD() {
        #if USE_SD
        Serial.print("Setting up SD card ");
        pinMode(SD_CS_PIN, OUTPUT);
        if (SD.begin(SD_CS_PIN)) {
            // find a file
            uint16_t fileIndex;
            for (fileIndex = 1; fileIndex; fileIndex++) {
                sprintf_P(path, PSTR("CD%05u.TXT"), fileIndex);
                if (!SD.exists(path)) {
                    break;
                }
            }
            if (fileIndex == 0) {
                sdReady = false;
                Serial.println("NOK");
            }
            sdReady = true;
            Serial.println("OK");
            Serial.print("card size ");
            Serial.println(SD.cardSize());
            Serial.print("using file ");
            Serial.println(path);
        } else {
            sdReady = false;
            Serial.println("NOK");
        }
        #endif
    }

};

CarDuino carDuino;
int pollInterval = 3000;

void sendData(char* vin, TRIBER_GPS_DATA* gpsData, TRIBER_MEMS_DATA* memsData, TRIBER_OBD_DATA* obdData);

void setup() {
    Serial.begin(115200);
    delay(5000);
    Serial.println("Freematics ONE+");
    Serial.println("Setting up");
    delay(1000);
    // init LED pin
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, HIGH);
    byte ver = carDuino.begin();
    Serial.print("Firmware Ver. ");
    Serial.println(ver);
    carDuino.setup();
    digitalWrite(PIN_LED, LOW);
}

void loop() {
    digitalWrite(PIN_LED, HIGH);
    unsigned long before = millis();
    TRIBER_GPS_DATA gpsData = carDuino.readGPS();
    TRIBER_MEMS_DATA memsData = carDuino.readMEMS();
    TRIBER_OBD_DATA obdData = carDuino.readOBD();
    sendData(carDuino.vin, &gpsData, &memsData, &obdData);
    unsigned long after = millis();
    long sleepDuration = (pollInterval + 10) - (after-before);
    Serial.print("Cycle took ");
    Serial.println(after-before);
    digitalWrite(PIN_LED, LOW);
    if(sleepDuration > 0) {
        Serial.print("Sleeping for ");
        Serial.println(sleepDuration);
        delay(sleepDuration);
    }
    Serial.println();
}

void sendData(char* vin, TRIBER_GPS_DATA* gpsData, TRIBER_MEMS_DATA* memsData, TRIBER_OBD_DATA* obdData) {
    StaticJsonBuffer<650> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["vin"] = vin;

    if(!gpsData->empty) {
        JsonObject& gpsJson = root.createNestedObject("gps");
        gpsJson["date"] = gpsData->date;
        gpsJson["time"] = gpsData->time;
        gpsJson["lat"] = gpsData->lat;
        gpsJson["long"] = gpsData->lng;
        gpsJson["alt"] = gpsData->alt;
        gpsJson["speed"] = gpsData->speed;
        gpsJson["sat"] = gpsData->sat;
        gpsJson["heading"] = gpsData->heading;
    }
    if(!memsData->empty) {
        JsonObject& memsJson = root.createNestedObject("mems");
        memsJson["x"] = memsData->x;
        memsJson["y"] = memsData->y;
        memsJson["z"] = memsData->z;
    }
    if(!obdData->empty) {
        JsonObject& obdJson = root.createNestedObject("obd");
        obdJson["load"] = obdData->load;
        obdJson["coolant"] = obdData->coolant;
        obdJson["rpm"] = obdData->rpm;
        obdJson["speed"] = obdData->speed;
        obdJson["intake-temp"] = obdData->intakeTemp;
        obdJson["throttle"] = obdData->throttle;
        obdJson["aux"] = obdData->aux;
        obdJson["runtime"] = obdData->runtime;
        obdJson["fuel"] = obdData->fuel;
        obdJson["baro"] = obdData->baro;
        obdJson["temp"] = obdData->temp;
        obdJson["pedal"] = obdData->pedal;
        obdJson["bat"] = obdData->bat;
    }
    root.prettyPrintTo(Serial);
    Serial.println();
    String jsonString;
    root.printTo(jsonString);
    carDuino.writeToSD(jsonString);
    carDuino.sendWifi(jsonString);
}